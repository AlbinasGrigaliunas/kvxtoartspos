﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using VRPS.Service.Plugin.RetailSuite.ArtsPosLogConverter;

namespace KvxConverter
{
    
    public class KvxToArtPosLog: IJob
    {
        private static readonly log4net.ILog log = LogHelper.GetLogger();
        string _kvxPath = Properties.Settings.Default.kvxPath;
        string _artsPosLogPath = Properties.Settings.Default.ArtsPosLogPath;
        string _kvxArchPath = Properties.Settings.Default.kvxArchPath;
        int _pageSize = Properties.Settings.Default.PageSize;
        string _constring = Properties.Settings.Default.ConnectionString;
        
        string suffix = "GZIP";
        string prefix = "GZIP";
        
        public void Execute(IJobExecutionContext context)
        {
            ConvertKvxToArtPos();
        }
        private void CreateIfMissing(string path)
        {
            bool folderExsists = Directory.Exists(path);
            if (!folderExsists) Directory.CreateDirectory(path);
        }
        public void ConvertKvxToArtPos()
        {
            log.Info("KvxToArtPosLog is starting");

            CreateIfMissing(_kvxPath);
            CreateIfMissing(_kvxArchPath);
            CreateIfMissing(_artsPosLogPath);
            
            // migrate receipt xml from database
            GetDataFromDB();
            // migrate receipt xml from xml files
           // GetDataFroFileSystem();

        }
        private void UpdateRowValue(long rowId)
        {
            using (SqlConnection sqlCon = new SqlConnection(_constring))
            {                            
                using (SqlCommand sqlcmd = new SqlCommand("[migration].[csp_LastModified_data]", sqlCon))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = "dbo.Receipts";
                    sqlcmd.Parameters.Add("@LastDateModified", SqlDbType.DateTime).Value = DateTime.Now;
                    sqlcmd.Parameters.Add("@LastModifiedId", SqlDbType.BigInt).Value = rowId;
                    sqlCon.Open();
                    sqlcmd.ExecuteNonQuery();
                }
            }

        }
        private long GetMaxRowId()
        {
            long maxRowid=0;
            using (SqlConnection sqlCon = new SqlConnection(_constring))
            {
                using (SqlCommand sqlcmd = new SqlCommand("Select [LastModifiedId] from [migration].[Data_migration_updates] where Table_Name = @tableName", sqlCon))
                {
                    sqlcmd.Parameters.Add(new SqlParameter("tableName", "dbo.Receipts"));
                    sqlCon.Open();
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.IsDBNull(0)) { break; }
                            maxRowid = (long)reader[0];
                        }
                    }

                }              
            }
            return maxRowid;
        }
        private void GetDataFromDB()
        {
            long lastProcessedRowId = GetMaxRowId();
            long maxProcessedRowId = lastProcessedRowId;
            using (SqlConnection sqlcon = new SqlConnection())
            {
                sqlcon.ConnectionString = _constring;              
                SqlCommand cmd = new SqlCommand("Select top(" + _pageSize + ") rowId, " +
                    "ReceiptXML.value('(/RetailCenter/Store/ReceiptPosInfo)[1]', 'varchar(max)') as kvxFile " +
                    "from dbo.Receipt with (nolock) " +
                    "where ReceiptDateTime >= @date and RowId > @maxRowId " +
                    "order by RowId asc", sqlcon);
                cmd.Parameters.Add(new SqlParameter("date", "2018.01.01"));
                cmd.Parameters.Add(new SqlParameter("maxRowId", lastProcessedRowId));
                    sqlcon.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                    while (reader.Read())
                    {
                        long rowId;
                        rowId = (long)reader[0];
                        maxProcessedRowId = rowId;
                        if (reader.IsDBNull(1))
                        {
                            log.Warn("No kvx-file in row - " + rowId);
                            continue;
                        }
                        XmlDocument xdoc = new XmlDocument();
                        int storeId;
                        short posId;
                        int receiptId;
                        int artpos;

                        log.Info("Processing row " + rowId);
                        try
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                Converter.Convert(Decompress(Encoding.UTF8, reader[1].ToString(), prefix, suffix), ms, out storeId, out posId, out receiptId, out artpos);
                                ms.Position = 0;
                                StreamReader sr = new StreamReader(ms);
                                XmlDocument artPosLogXml = new XmlDocument();
                                artPosLogXml.LoadXml(sr.ReadToEnd());
                                ChangeAttWriteToFile(rowId, artPosLogXml, storeId);
                            }

                        }
                        catch (Exception ex)
                        {
                            log.Error("Error processing row - " + rowId +". "+ ex.Message);
                        }
                    }
                    if (maxProcessedRowId!= lastProcessedRowId)
                    {
                        UpdateRowValue(maxProcessedRowId);
                    }

                }
            }
        }
        public void GetDataFroFileSystem()
        {
            foreach (string file in Directory.EnumerateFiles(_kvxPath, "*.xml"))
            {

                XmlDocument xdoc = new XmlDocument();
                int rowId;
                int storeId;
                short posId;
                int receiptId;
                int artpos;

                Console.WriteLine("Processing file " + file + ". " + DateTime.Now);
                xdoc.Load(file);
                foreach (XmlNode xnode in xdoc.DocumentElement.ChildNodes)
                {
                    if (!xnode.LastChild.Name.Equals("kvxFile", StringComparison.InvariantCultureIgnoreCase) ||
                        !xnode.FirstChild.Name.Equals("rowId", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }


                    rowId = Int32.Parse(xnode.FirstChild.InnerText);
                    Console.WriteLine("Processing row " + rowId + ". " + DateTime.Now);
                    try
                    {
                        //string kvx = Decompress(Encoding.UTF8, xnode.LastChild.InnerText, prefix, suffix);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            Converter.Convert(Decompress(Encoding.UTF8, xnode.LastChild.InnerText, prefix, suffix), ms, out storeId, out posId, out receiptId, out artpos);
                            ms.Position = 0;
                            StreamReader sr = new StreamReader(ms);
                            XmlDocument artPosLogXml = new XmlDocument();
                            artPosLogXml.LoadXml(sr.ReadToEnd());
                            ChangeAttWriteToFile(Int32.Parse(xnode.FirstChild.InnerText), artPosLogXml, storeId);
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }

                string path = Path.Combine(_kvxArchPath, Path.GetFileName(file));

                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                File.Move(file, path);
            }
        }

        private void ChangeAttWriteToFile(long rowId, XmlDocument xmlDoc, int storeId)
        {
            string ns = "http://schemas.vismaretail.com/poslog/";
            XmlAttribute xmlAtt = xmlDoc.CreateAttribute("VRArtsPoslogExt", "Origin", ns);
            xmlAtt.Value = "KVXToArtsConverter";
            XmlNode node = xmlDoc.DocumentElement.FirstChild;
            node.Attributes.Append(xmlAtt);

            XmlNodeList xel = xmlDoc.GetElementsByTagName("UnitID");
            xel[0].InnerText = storeId.ToString();
            
            xmlDoc.Save(_artsPosLogPath + rowId.ToString() + ".xml");
        }
        private String Decompress(Encoding encoding, string inputString, string PrefixForCompressedString, string SuffixForCompressedString)
        {
            string str = inputString;
            string DecompressedString = string.Empty;
            if (str.Length > 0 & str.Length > PrefixForCompressedString.Length + SuffixForCompressedString.Length)
            {
                if (PrefixForCompressedString.Length > 0)
                    str = inputString.Substring(PrefixForCompressedString.Length, str.Length - PrefixForCompressedString.Length);
                if (SuffixForCompressedString.Length > 0)
                    str = str.Substring(0, str.Length - SuffixForCompressedString.Length);
            }
            if (str.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                string decStr = string.Empty;
                try
                {
                    using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(str)))
                    {
                        using (GZipStream gzipStream = new GZipStream((Stream)memoryStream, CompressionMode.Decompress))
                        {
                            byte[] buffer = new byte[4];
                            memoryStream.Position = memoryStream.Length - 4L;
                            memoryStream.Read(buffer, 0, 4);
                            int int32 = BitConverter.ToInt32(buffer, 0);
                            if (int32 < 1000000)
                            {
                                memoryStream.Position = 0L;
                                byte[] numArray = new byte[int32];
                                gzipStream.Read(numArray, 0, int32);
                                decStr = encoding.GetString(numArray);
                            }
                            else
                                decStr = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    DecompressedString = decStr;
                }

            }
            return DecompressedString;
        }
    }
}
