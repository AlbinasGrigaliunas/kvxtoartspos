﻿using Quartz;
using Quartz.Impl;
using System;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace KvxConverter
{
    class Program
    {
      
        static void Main(string[] args)
        {
            int triggerIntervalMin = Properties.Settings.Default.TriggerMin;
            try
            {               
                ISchedulerFactory schedFac = new StdSchedulerFactory();
                IScheduler scheduler = schedFac.GetScheduler();
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<KvxToArtPosLog>()
                    .WithIdentity("myJob", "group1")
                    .Build();
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("myTrigger", "group1")
                    .StartNow()
                    .WithSimpleSchedule(x => x.WithIntervalInMinutes(triggerIntervalMin).RepeatForever())
                    .Build();

                scheduler.ScheduleJob(job, trigger);             
            }
            catch(SchedulerException se)
            {
                Console.WriteLine(se);
            }
            

        }

       
    }
}
